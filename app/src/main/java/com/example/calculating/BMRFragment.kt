package com.example.calculating

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.calculating.databinding.FragmentBmrBinding
import com.example.calculating.model.Everything


class BMRFragment : Fragment() {
    private var binding:FragmentBmrBinding? = null
    private val sharedViewModel: Everything by activityViewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View{

        val fragmentBinding = FragmentBmrBinding.inflate(inflater, container, false)
        binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply{
            viewModel =sharedViewModel
            btncal.setOnClickListener{
                sumBMR()
                goToAnsBMR()
            }
        }
    }

    private fun goToAnsBMR() {
        findNavController().navigate(R.id.action_BMRFragment_to_ansBMRFragment)

    }


    private  fun sumBMR() {
        val weight = binding?.editinputweight?.text.toString().toIntOrNull()
        if (weight != null) {
            sharedViewModel.setWeight(weight)
        } else {
            ShowError()
        }
        val height = binding?.editinputheight?.text.toString().toIntOrNull()
        if (height != null) {
            sharedViewModel.setHeight(height)
        } else {
            ShowError()
        }
        val age = binding?.editinputage?.text.toString().toIntOrNull()
        if (age != null) {
            sharedViewModel.setAge(age)
        }else{
            ShowError()
        }
        val selectId = binding?.radioGroup?.checkedRadioButtonId
        val pickbutton = when (selectId) {
            R.id.radiomale -> 66
            else -> 665
        }
        if ( pickbutton != null){
            sharedViewModel.setsex(pickbutton)
        }else{
            ShowError()
        }


    }
    fun ShowError(){
        Toast.makeText(context,  "Please complete the information.", Toast.LENGTH_SHORT)
            .show()
    }

    override fun onDestroyView() {

        super.onDestroyView()
        binding = null
    }


}





