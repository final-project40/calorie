package com.example.calculating

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.calculating.databinding.FragmentMeaningBinding


class MeaningFragment : Fragment() {
    private var binding : FragmentMeaningBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val fragmentBinding =  FragmentMeaningBinding.inflate(inflater,container,false)
        binding = fragmentBinding
        return fragmentBinding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            // Set up the button click listeners
            btnnext.setOnClickListener {goToBMR()}
        }
    }
    private fun goToBMR(){
        findNavController().navigate(R.id.action_meaningFragment_to_BMRFragment)
    }
    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}