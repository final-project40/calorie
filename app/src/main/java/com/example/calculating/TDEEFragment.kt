package com.example.calculating

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.calculating.databinding.FragmentTdeeBinding
import com.example.calculating.model.Everything


class TDEEFragment : Fragment() {
    private var binding : FragmentTdeeBinding? = null
    private val sharedViewModel: Everything by activityViewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val fragmentBinding =  FragmentTdeeBinding.inflate(inflater,container,false)
        binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            viewModel =sharedViewModel
            // Set up the button click listeners
            btncaltdee.setOnClickListener {
                sumTDEE()
                goToAnsTDEE()
            }
        }
    }
    private  fun sumTDEE() {
        val pickbtnn = when (binding?.activity?.checkedRadioButtonId) {
            R.id.option_never -> 1.2
            R.id.option_occasionally -> 1.375
            R.id.option_usually -> 1.55
            R.id.option_sometimes -> 1.725
            else -> 1.9
        }
            sharedViewModel.setActi(pickbtnn)


    }
    fun goToAnsTDEE(){
        findNavController().navigate(R.id.action_TDEEFragment_to_ansTDEEFragment)
    }
    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}