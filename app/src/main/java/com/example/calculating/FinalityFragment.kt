package com.example.calculating

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.calculating.databinding.FragmentFinalityBinding
import com.example.calculating.model.Everything


class FinalityFragment : Fragment() {
    private var binding : FragmentFinalityBinding? = null
    private val sharedViewModel: Everything by activityViewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentBinding =  FragmentFinalityBinding.inflate(inflater,container,false)
        binding = fragmentBinding
        return  fragmentBinding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            viewModel=sharedViewModel
            // Set up the button click listeners
            btnbb.setOnClickListener { goToStart() }
        }
    }
    fun  goToStart(){
        findNavController().navigate(R.id.action_finalityFragment_to_startFragment)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}