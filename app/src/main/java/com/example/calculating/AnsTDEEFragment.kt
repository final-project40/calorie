package com.example.calculating

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.calculating.databinding.FragmentAnsTDEEBinding
import com.example.calculating.model.Everything


class AnsTDEEFragment : Fragment() {

    private val sharedViewModel: Everything by activityViewModels()
    private var binding : FragmentAnsTDEEBinding? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val fragmentBinding =  FragmentAnsTDEEBinding.inflate(inflater,container,false)
        binding = fragmentBinding
        return fragmentBinding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            viewModel =sharedViewModel
            // Set up the button click listeners
            btnnextfinality.setOnClickListener { goToFinality() }
        }
    }
    fun goToFinality(){
        findNavController().navigate(R.id.action_ansTDEEFragment_to_finalityFragment)
    }


    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}