package com.example.calculating.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

//นั่งทำงานอยู่กับที่ และไม่ได้ออกกำลังกายเลย = BMR x 1.2
//ออกกำลังกายหรือเล่นกีฬาเล็กน้อย ประมาณอาทิตย์ละ 1-3 วัน = BMR x 1.375
//ออกกำลังกายหรือเล่นกีฬาปานกลาง ประมาณอาทิตย์ละ 3-5 วัน = BMR x 1.55
//ออกกำลังกายหรือเล่นกีฬาอย่างหนัก ประมาณอาทิตย์ละ 6-7 วัน = BMR x 1.725
//ออกกำลังกายหรือเล่นกีฬาอย่างหนักทุกวันเช้าเย็น = BMR x 1.9

class Everything : ViewModel() {

    private var _bmr = MutableLiveData<Int>()
    val bmr: LiveData<Int> = _bmr

    private var _tdee = MutableLiveData<Int>()
    val tdee: LiveData<Int> = _tdee

    private var _weight = MutableLiveData<Int>()

    val weight: LiveData<Int> = _weight

    private var _height = MutableLiveData<Int>()
    val height: LiveData<Int> = _height

    private var _age = MutableLiveData<Int>()
    val age: LiveData<Int> = _age

    private var _sex = MutableLiveData<Int>() // ค่าของปุ่ม

    private var _acti = MutableLiveData<Double>()
    val act : LiveData<Double> = _acti

    private var _pickbtn = MutableLiveData<String>() // ตัวอักษร
    val pickbtn: LiveData<String> = _pickbtn

    fun  setActi(ansActi : Double){
        _acti.value = ansActi
        setSumTdee()
    }

    fun setWeight(ansWeight: Int) {
        _weight.value = ansWeight
    }

    fun setHeight(ansHeight: Int) {
        _height.value = ansHeight

    }

    fun setAge(ansAge: Int) {
        _age.value = ansAge
    }

    fun setsex(ansSex: Int) {
        _sex.value = ansSex
        setSumMbr()
    }

    fun setPicbtn(ansString: String) {
        _pickbtn.value = ansString

    }
//    สำหรับผู้ชาย : BMR = 66 + (13.7 x น้ำหนักตัวเป็น กก.) + (5 x ส่วนสูงเป็น ซม.) – (6.8 x อายุ)
//    สำหรับผู้หญิง : BMR = 665 + (9.6 x น้ำหนักตัวเป็น กก.) + (1.8 x ส่วนสูงเป็น ซม.) – (4.7 x อายุ)
      fun setSumMbr() {
           if(_sex.value?.equals(66) == true){
           _bmr.value =(66+((13.7*weight.value!!))+((5*height.value!!))-(6.8 *age.value!!)).toInt()
       }else{
           _bmr.value =(665+((9.6*weight.value!!))+((1.8*height.value!!))-(4.7 *age.value!!)).toInt()
       }
    }
    fun setSumTdee(){
        val detail_t = (act.value)
        if (detail_t != null) {
            _tdee.value = (detail_t * bmr.value!!).toInt()
        }
    }

}



















