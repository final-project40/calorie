package com.example.calculating

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.calculating.databinding.FragmentAnsBMRBinding
import com.example.calculating.model.Everything


class AnsBMRFragment : Fragment() {

    private var binding : FragmentAnsBMRBinding? = null
    private val sharedViewModel: Everything by activityViewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val fragmentBinding =  FragmentAnsBMRBinding.inflate(inflater,container,false)
        binding = fragmentBinding
        return fragmentBinding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            viewModel = sharedViewModel
            btntdee.setOnClickListener { goToTDEE() }
            btncats.setOnClickListener { goToStart() }
        }
    }
    private fun goToTDEE(){
        findNavController().navigate(R.id.action_ansBMRFragment_to_TDEEFragment)
    }
    private fun goToStart(){
        findNavController().navigate(R.id.action_ansBMRFragment_to_startFragment)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

}